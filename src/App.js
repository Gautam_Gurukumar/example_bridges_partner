import logo from './logo.svg';
import './App.css';
import './GoodboxBridge.js' ;
import {getValue, saveValue, showToast} from "./GoodboxBridge";

function App() {
    function onSave() {

        saveValue("testKey", "testValue2")

    }

    function onGet() {

        getValue("testKey")

    }

    return (
        <div className="App">
            <header className="App-header">
                <button onClick={onSave}>Save
                </button>
                <button onClick={onGet}>Get
                </button>
            </header>
        </div>
    );
}

export default App;
