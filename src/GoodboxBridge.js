export const saveValue = (key, value) => {

    window.webViewBridge.send("saveValue", {key: key, value: value}, (res) => {
        showToast("success save: ",res)
    }, err => {
    })
}

export const getValue = (key) => {

    window.webViewBridge.send("getValue", {key: key}, (res) => {
        showToast(`success get: ${res}`)
    }, err => {
    })

    // return window.ReactNativeWebView.postMessage(JSON.stringify({method: "saveValue", key: key, value: value}))
}


export const showToast = (toast)=>{
    window.webViewBridge.send("showToast", {toast: toast}, (res) => {
        console.log("Result: ",res)
    }, err => {
    })
    // return window.ReactNativeWebView.postMessage(toast);
}
